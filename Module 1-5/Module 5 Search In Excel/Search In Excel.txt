Module 5: Introduction To Excel .Find Function
    - SearchOrder [Row,Column]
    - LookAt [whole,part]
    - MatchCase [True,False]
    - FindLookIn []

Exercise:
# Worksheet
[SearchOrder]
1. Find 'Total Registration' cell colored [yellow]
    - Extract Address and Value
2. Find 'Total Registration' cell colored [light green]
    - Extract Address and Value
[LookAt]
3. Find 'Total Registration' cell colored [light blue]
    - Extract Address and Value
[MatchCase]
4. Find 'Total Registration' cell colored [orange]
    - Extract Address and Value
[MatchCase]
5. Find 'Total Registration' cell colored [orange]
    - Extract Address and Value
6. Find '保 有 契 約 （ １２月末 ）' cell colored [dark orange]
    - Extract Address and Value
[Search Area Application]
7. Find 'Total Registration' cell colored [dark green]
    - Extract Address and Value
