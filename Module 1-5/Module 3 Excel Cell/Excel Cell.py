import win32com.client as xlx # Import win32
sf = xlx.gencache.EnsureDispatch("Excel.Application") # Initiate Excel Application
sf.DisplayAlerts = False # Avoid display any alert
sourceFilePath=r'C:\Users\ajaafar\OneDrive - Internet Securities, LLC\Desktop\Training\Module 1-5\Module 3 Excel Cell\1001_table41a_20190819.xls' # Source File Path
scBk = sf.Workbooks.Open(sourceFilePath, ReadOnly=True) # Open Source File
sf.Visible = True # Display source File
scsht = scBk.Worksheets('T4.1d')
def exercise_1():
    # Extract & display cell value at coordinate [Row=5,Column=13]
    cVal = scsht.Cells(5,13).Value
    print(cVal)
def exercise_2():
    # Extract & display cell value at coordinate [Row=5,Column=13]
    cTxt = scsht.Cells(5,13).Text
    print(cTxt)
def exercise_3():
    # Extract cell address at coordinate [Row=7,Column=12]
    cAdrs = scsht.Cells(7,12).Address
    print(cAdrs)
def exercise_4():
    # Extract cell row at coordinate [Row=7,Column=12] using .Row
    cAdrs=scsht.Cells(7, 12).Address
    cRng=scsht.Range(cAdrs) # Convert Address To Cell Range Object
    cRow=cRng.Row
    print(cRow)
def exercise_5():
    # Extract cell row at coordinate [Row=7,Column=12] using .Row
    cAdrs=scsht.Cells(7, 12).Address
    cRng=scsht.Range(cAdrs) # Convert Address To Cell Range Object
    cCol=cRng.Column
    print(cCol)
def exercise_6():
    # Convert column=12 to Alphabet string
    cAdrs = str(scsht.Cells(1,12).Address)
    cColAphbt = str(cAdrs).split('$')[1]
    print(cColAphbt)

