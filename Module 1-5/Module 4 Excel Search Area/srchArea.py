def StRtAdRs():
    # Valid Start Range Address to generate search area
    StRtAdRs = "CCRR"
    StRtAdRs = "$CC$RR"
    StRtAdRs = "{}".format(str(XRnG.Address)) # Ranged Object
    StRtAdRs = "$A${}".format(str(iR)) # Row Only
    StRtAdRs = "${}${}".format(str(sC),str(iR)) # Split Address & Row
    StRtAdRs = scsht.Cells(iR,iC).Address # Coordinate
def EnDAdRs():
    # Valid End Range Address to generate search area
    EnDAdRs = "CCRR"
    EnDAdRs = "$CC$RR"
    EnDAdRs = "{}".format(str(XRnG.Address)) # Ranged Object
    EnDAdRs = "$A${}".format(str(iR)) # Row Only
    EnDAdRs = "${}${}".format(str(sC),str(iR)) # Split Address & Row
    EnDAdRs = scsht.Cells(iR,iC).Address # Coordinate
def EnD():
    # End Of Used Range Value
    EnDiR = scsht.UsedRange.Rows.Count # e.g 1
    EnDiC = scsht.UsedRange.Columns.Count # e.g 9
    EnDAdRs=str(scsht.UsedRange.Address).split(':')[1] # e.g $N$44
    EnDsC=str(scsht.UsedRange.Columns.Address.split(':')[1]).split('$')[1] # eg. C
    EnDiR=str(scsht.UsedRange.Columns.Address.split(':')[1]).split('$')[2] # e.g 1
def RnG():
    RnG = "{}:{}".format(str(StRtAdRs), str(EnDAdRs))
    srchArea = scsht.Range(str(RnG))
