import win32com.client as xlx # Import win32
sf = xlx.gencache.EnsureDispatch("Excel.Application") # Initiate Excel Application
sf.DisplayAlerts = False # Avoid display any alert
sourceFilePath=r'C:\Users\ajaafar\OneDrive - Internet Securities, LLC\Desktop\Training\Module 1-5\Module 3 Excel Cell\1001_table41a_20190819.xls' # Source File Path
scBk = sf.Workbooks.Open(sourceFilePath, ReadOnly=True) # Open Source File
sf.Visible = True # Display source File
scsht = scBk.Worksheets('T4.1d')
def exercise_1():
    # Generate Search Area in Start Address=[Row=9,Column=9] until End Address=[Row=9,Column=14]
    cAdrsStrt=scsht.Cells(9, 9).Address
    cAdrsEnd=scsht.Cells(9, 14).Address
    RnG = '{}:{}'.format(cAdrsStrt, cAdrsEnd)
    srchArea = scsht.Range(RnG)
def exercise_2():
    # Generate Search Area in Start Address=[C9] until End Address=[H12]
    RnG = 'C9:H12'
    srchArea = scsht.Range(RnG)
def exercise_3():
    # Find End Used Row
    EnDiR = scsht.UsedRange.Rows.Count
def exercise_4():
    # Find End Used Column
    EnDiC = scsht.UsedRange.Columns.Counts
def exercise_5():
    # Generate Search Area Start Address=[C9] until End Address=[EndOfUsedRange]
    StRtAdRs = "C9"
    EnDAdRs=str(scsht.UsedRange.Address).split(':')[1]
    RnG = "{}:{}".format(str(StRtAdRs), str(EnDAdRs))
    srchArea = scsht.Range(str(RnG))

