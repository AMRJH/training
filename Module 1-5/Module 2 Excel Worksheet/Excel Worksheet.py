import win32com.client as xlx # Import win32
sf = xlx.gencache.EnsureDispatch("Excel.Application") # Initiate Excel Application
sf.DisplayAlerts = False # Avoid display any alert
sourceFilePath=r'C:\Users\ajaafar\OneDrive - Internet Securities, LLC\Desktop\Training\Module 1\source.xlsx' # Source File Path
scBk = sf.Workbooks.Open(sourceFilePath, ReadOnly=True) # Open Source File
sf.Visible = True # Display source File
def exercise_1():
    # Display All worksheet name available in The Excel
    for list_sht in scBk.Sheets:
        print(list_sht.Name)
def exercise_2():
    # Find & display worksheet Named 'T4.1b(2)' using '=='
    Sht = 'T4.1b(2)'
    for list_sht in scBk.Sheets:
        if str(Sht) == str(list_sht.Name):
            print(list_sht.Name)
            break
def exercise_3():
    # Find & display all worksheet Named 'T4.1b' using '.find'
    Sht = 'T4.1b'
    for list_sht in scBk.Sheets:
        if str(list_sht.Name).find(str(Sht)) > -1:
            print(list_sht.Name)
def exercise_4():
    # Open worksheet named 'T4.1b(2)' Using Index
    scsht = scBk.Worksheets(2)
def exercise_5():
    # Open worksheet named 'T4.1b(2)' Using Worksheet Name
    scsht = scBk.Worksheets('T4.1b(2)')
def exercise_6():
    # Go to worksheet named 'T4.1h(2)' Using '.Activate()'
    scsht = scBk.Worksheets('T4.1b(2)')
    scsht.Activate()